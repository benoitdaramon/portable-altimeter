/*
Author: Benoit d'Aramon
Date: 26/04/2017
Comment: Code for the arduino-base Altimeter
*/


#include "TM1637.h"
#include <Wire.h>
#include "SparkFunMPL3115A2.h"

/* Connexions for Arduino nano*/
//push button -> D4
/* 7 digits diplay TM1637 */
//DIO -> D5
//CLK -> D7
/* pressure sensor MPL3115A2 */
//SCL -> A5
//SDA -> 4A

MPL3115A2 myPressure;
TM1637 tm1637(7 /*CLK*/, 5/*DIO*/);
int altitude_max;
int altitude_offset=0;

void setup () {
  pinMode(4, INPUT); //push button
  
  tm1637.init();
  tm1637.set(7);    //BRIGHT_TYPICAL = 2,BRIGHT_DARKEST = 0,BRIGHTEST = 7;
  tm1637.point(1);

    Wire.begin();        // Join i2c bus
  Serial.begin(9600);  // Start serial for output

  myPressure.begin(); // Get sensor online

  //Configure the sensor
  myPressure.setModeAltimeter(); // Measure altitude above sea level in meters

  myPressure.setOversampleRate(7); // Set Oversample to the recommended 128
  myPressure.enableEventFlags(); // Enable all three pressure and temp event flags 
  altitude_offset = myPressure.readAltitude();
}

void loop () {      

  int altitude = myPressure.readAltitude() - altitude_offset;
  if (altitude < 0) {
    altitude = 0;
  }
  Serial.print("Altitude(m):");
  Serial.print(altitude);
  Serial.println();
  if (altitude > altitude_max){
    altitude_max = altitude;
  }
  int display_altitude_max = digitalRead(4);
  if (display_altitude_max == HIGH){
    altitude=altitude_max;
  }
  tm1637.display(0,uint8_t(altitude/1000));
  tm1637.display(1,uint8_t((altitude/100)%10));
  tm1637.display(2,uint8_t((altitude/10)%10));
  tm1637.display(3,uint8_t(altitude%10));
}
